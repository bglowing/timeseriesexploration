library(dplyr)
library(data.table)
library(ggplot2)

Pricing <- fread('Pricing.txt')
Pricing[, ObsDateTime := as.POSIXct(StartTimeStamp, '%Y-%m-%d %H:%M:%S', tz = 'UTC')]
summary(Pricing)
head(Pricing)

p <- ggplot() +
  geom_line(data = Pricing, aes(x = ObsDateTime, y = LandedPrice, group = Asin, color = Asin))
p


AsinMeans <- Pricing %>%
  group_by(Asin) %>%
  summarise(LandedPriceMean = mean(LandedPrice))

Pricing <- inner_join(Pricing, AsinMeans)
Pricing$NormalizedLandedPrice <- Pricing$LandedPrice / Pricing$LandedPriceMean

ggplot() + geom_line(data = Pricing, aes(x = ObsDateTime, y = NormalizedLandedPrice, group = Asin, color = Asin))


ggplot() + geom_line(data = Pricing[Pricing$Asin == 'B00NJ0H10G',], aes(x = ObsDateTime, y = NormalizedLandedPrice, group = Asin, color = Asin))
